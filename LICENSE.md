## License

```
Copyright © 2018-2020, hacker1024 @ GitHub.com. All rights reserved. Contact via email: hacker1024@users.sourceforge.net
```

This repository exists to allow you to view this application's code; you are not permitted to redistribute it.

Pull requests are permitted; by submitting one, you'll be giving me, hacker1024, all rights to the submitted code.

The licensing terms are as such because I, hacker1024, may sell this app as a product in the future.
Depending on the product's success if I do so, I may relicense this app under a more permissive license (such as the GPL).

I, hacker1024, may change this license at any time.
